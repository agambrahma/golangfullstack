package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strconv"
	"testing"
)

func TestGetBirdsHandler(t *testing.T) {
	mockStore := InitMockStore()

	mockStore.On("GetBirds").Return([]*Bird{
		{"Barn Owl", "Really the best owl!"},
	}, nil).Once()

	req, err := http.NewRequest("GET", "", nil)
	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	hf := http.HandlerFunc(getBirdHandler)
	hf.ServeHTTP(recorder, req)

	if status := recorder.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v, want OK",
			status)
	}

	expected := Bird{"Barn Owl", "Really the best owl!"}
	b := []Bird{}

	if err = json.NewDecoder(recorder.Body).Decode(&b); err != nil {
		t.Fatal(err)
	}

	actual := b[0]
	if actual != expected {
		t.Errorf("Unexpected body: got %v, want %v", actual, expected)
	}

	mockStore.AssertExpectations(t)
}

func TestCreateBirdsHandler(t *testing.T) {
	mockStore := InitMockStore()

	mockStore.On("CreateBird", &Bird{"Bird of paradise", "Not really"}).Return(nil)

	form := newCreateBirdForm()
	req, err := http.NewRequest("POST", "", bytes.NewBufferString(form.Encode()))
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(form.Encode())))

	if err != nil {
		t.Fatal(err)
	}

	recorder := httptest.NewRecorder()
	hf := http.HandlerFunc(createBirdHandler)
	hf.ServeHTTP(recorder, req)

	if status := recorder.Code; status != http.StatusFound {
		t.Errorf("Handler returned wrong status code: got %v, want Found", status)
	}

	mockStore.AssertExpectations(t)
}

func newCreateBirdForm() *url.Values {
	form := url.Values{}
	form.Set("species", "Bird of paradise")
	form.Set("description", "Not really")
	return &form
}

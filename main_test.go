package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRouter(t *testing.T) {
	r := newRouter()
	mockServer := httptest.NewServer(r)

	resp, err := http.Get(mockServer.URL + "/hello")

	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Handler got bad status code: got %v, want %v", resp.StatusCode, http.StatusOK)
	}
	defer resp.Body.Close()

	// Read out body.
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	actual := string(b)
	expected := `Hello, World!`

	if actual != expected {
		t.Errorf("Handler got bad body: got %v, want %v", actual, expected)
	}
}

func TestRouterForNonExistentRoute(t *testing.T) {
	r := newRouter()
	mockServer := httptest.NewServer(r)
	resp, err := http.Post(mockServer.URL+"/hello", "", nil)

	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusMethodNotAllowed {
		t.Errorf("Status should be 405, got %d", resp.StatusCode)
	}
}

func TestStaticFileServer(t *testing.T) {
	mockServer := httptest.NewServer(newRouter())
	resp, err := http.Get(mockServer.URL + "/assets/")
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Got bad error code: %d", resp.StatusCode)
	}

	actualContentType := resp.Header.Get("Content-Type")
	expectedContentType := "text/html; charset=utf-8"

	if actualContentType != expectedContentType {
		t.Errorf("Bad content type: want %s, got %s", expectedContentType, actualContentType)
	}
}

package main

import (
	"database/sql"
	"testing"

	_ "github.com/lib/pq"
	"github.com/stretchr/testify/suite"
)

type StoreSuite struct {
	suite.Suite

	store *dbStore
	db    *sql.DB
}

func (s *StoreSuite) SetupSuite() {
	connString := "dbname=bird_encyclopedia sslmode=disable"
	db, err := sql.Open("postgres", connString)
	if err != nil {
		s.T().Fatal(err)
	}
	s.db = db
	s.store = &dbStore{db: db}
}

func (s *StoreSuite) TearDownSuite() {
	s.db.Close()
}

func (s *StoreSuite) SetupTest() {
	_, err := s.db.Query("DELETE FROM birds")
	if err != nil {
		s.T().Fatal(err)
	}
}

// Go-visible test
func TestStoreSuite(t *testing.T) {
	s := &StoreSuite{}
	suite.Run(t, s)
}

func (s *StoreSuite) TestCreateBird() {
	s.store.CreateBird(&Bird{
		Species:     "test species",
		Description: "test description",
	})

	res, err := s.db.Query("SELECT COUNT(*) from birds WHERE species='test species' AND description='test description'")
	if err != nil {
		s.T().Fatal(err)
	}

	var count int
	for res.Next() {
		err := res.Scan(&count)
		if err != nil {
			s.T().Fatal(err)
		}
	}

	if count != 1 {
		s.T().Errorf("Unexpected count: got %d, expected 1", count)
	}
}

func (s *StoreSuite) TestGetBird() {
	_, err := s.db.Query("INSERT INTO birds(species, description) VALUES('pigeon', 'house bird')")
	if err != nil {
		s.T().Fatal(err)
	}

	birds, err := s.store.GetBirds()
	if err != nil {
		s.T().Fatal(err)
	}

	nBirds := len(birds)
	if nBirds != 1 {
		s.T().Errorf("Unexpected count: got %d, expected 1", nBirds)
	}

	expectedBird := Bird{
		Species:     "pigeon",
		Description: "house bird",
	}
	if *birds[0] != expectedBird {
		s.T().Errorf("Unexpected bird: expected %v, got %v", expectedBird, *birds[0])
	}
}
